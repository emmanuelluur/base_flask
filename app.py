from flask import Flask # importa Flask
from flask import request, render_template
# instaniamos objeto Flask
app = Flask(__name__, template_folder = 'views') # nombramos <<views>> como carpeta para las vistas

@app.route("/") # se usa un decorador para las rutas
def index():
    """ Una función para retornar las respuestas """
    return render_template('index.html', name = "Emmanuel") # pasamos un parametro a la vista principal

@app.route("/user/<int:id>", methods={'GET'}) # metodo GET INT parametro
def user(id=None):
    if id == None:
        return "No se encontro ID"
    return "Hello {}".format(id)

@app.route("/username/<name>", methods={'GET'}) # metodo GET str parametro
def username(name=None):
    if name == None:
        return "No se encontro nombre"
    return "Hello {}".format(name)

""" 404 HANDLER """
@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return 'Page not found 404', 404

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
